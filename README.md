<h1>Projet de cours UQAC - Colorless</h1>

<p><h2>Description du travail</h2>
Prototype de jeu de type FPS où le joueur peut acheter des compétences avec des 
points récupérés sur des ennemis ou se soigner avec. Le joueur ne peut pas interagir
avec les objets tant qu'il n'a pas tué tous les ennemis de la zone et ainsi recoloré
cette zone.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Visual Studio
    <li>Unity3D
    <li>C#
    <li>Git
</ul></p>

<p><h2>Méthode de travail</h2>
<ul><li>Méthodologie Agile : SCRUM
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Level designer
    <li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Ludovic Jozereau
    <li>Benjamin Laschkar
    <li>Jeremy Rasendrason
</ul></p>

[Accès vers l'exécutable](https://drive.google.com/drive/folders/1XHtTR5jC654m7lwEamsyX4VTd4qZuNK-)